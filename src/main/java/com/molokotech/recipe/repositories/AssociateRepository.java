package com.molokotech.recipe.repositories;
import com.molokotech.recipe.models.Associate;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AssociateRepository extends MongoRepository<Associate, Object> {

}
