package com.molokotech.recipe.services;
import com.molokotech.recipe.models.Recipe;
import com.molokotech.recipe.repositories.RecipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class RecipeService {
    @Autowired
    RecipeRepository recipeRepository;

    public Recipe createRecipe(Recipe recipe) {
        return recipeRepository.save(recipe);
    }

    public Optional<Recipe> getRecipe(Long id) {
        return recipeRepository.findById(id);
    }
}
