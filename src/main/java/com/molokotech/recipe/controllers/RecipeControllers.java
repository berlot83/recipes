package com.molokotech.recipe.controllers;
import com.molokotech.recipe.models.Associate;
import com.molokotech.recipe.models.Medic;
import com.molokotech.recipe.models.Recipe;
import com.molokotech.recipe.services.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/recipe")
public class RecipeControllers {

    @Autowired
    RecipeService recipeService;

    @GetMapping("/{id}")
    public Recipe getRecipe(@PathVariable Long id) {
        return recipeService.getRecipe(id).orElseThrow();
    }

    @PostMapping("/")
    public Recipe createRecipe(@RequestBody Recipe recipe) {
        return recipeService.createRecipe(recipe);
    }


}
