package com.molokotech.recipe.repositories;
import com.molokotech.recipe.models.Medic;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicRepository extends MongoRepository<Medic, Object> {

}
