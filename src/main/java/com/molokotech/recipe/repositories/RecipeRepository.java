package com.molokotech.recipe.repositories;
import com.molokotech.recipe.models.Recipe;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecipeRepository extends MongoRepository<Recipe, Object> {

}
