package com.molokotech.recipe.services;

import com.molokotech.recipe.models.Associate;
import com.molokotech.recipe.repositories.AssociateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AssociateService {
    @Autowired
    AssociateRepository associateRepository;

    public Associate createAssociate(Associate associate) {
        return associateRepository.save(associate);
    }

    public Optional<Associate> getAssociate(Long id) {
        return associateRepository.findById(id);
    }

}
