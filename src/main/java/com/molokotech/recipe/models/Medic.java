package com.molokotech.recipe.models;

public class Medic extends Person {
    Long cuit;
    Long licence;

    public Long getCuit() {
        return cuit;
    }

    public void setCuit(Long cuit) {
        this.cuit = cuit;
    }

    public Long getLicence() {
        return licence;
    }

    public void setLicence(Long licence) {
        this.licence = licence;
    }
}
