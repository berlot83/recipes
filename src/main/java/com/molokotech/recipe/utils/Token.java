package com.molokotech.recipe.utils;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import javax.crypto.SecretKey;
import java.util.Date;

public class Token {
    private static String getJWTToken(String recipeText) {
        SecretKey secretKey = Keys.secretKeyFor(SignatureAlgorithm.HS512);

        return Jwts
                .builder()
                .setId("Molokotech")
                .setSubject("Medic")
                .claim("recipe", recipeText)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 600000))
                .signWith(SignatureAlgorithm.HS512, secretKey).compact();
    }

    public static void main(String[] args) {
        System.out.println(getJWTToken("Paracetamol 150 ml, amoxidal 500 mg, miconazol 20gr"));
    }
}
