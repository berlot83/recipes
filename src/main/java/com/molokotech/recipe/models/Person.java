package com.molokotech.recipe.models;

public class Person {
    String name;
    String lastname;
    Long document;
    String regularPhone;
    String cellphone;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Long getDocument() {
        return document;
    }

    public void setDocument(Long document) {
        this.document = document;
    }

    public String getRegularPhone() {
        return regularPhone;
    }

    public void setRegularPhone(String regularPhone) {
        this.regularPhone = regularPhone;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }
}
